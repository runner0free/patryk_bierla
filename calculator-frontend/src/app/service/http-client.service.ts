import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

export class Calculate {
  constructor(
    public dailyPayGross: number,
    public codeCountry: string) {
  }
}

export class Country {
  constructor(
    public code: string,
    private name: string,
    public tax: number,
    public fixedCosts: number,
    public currencyCode: string) {
  }
}


@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(private httpClient: HttpClient) {
  }

  public getCountries() {
    console.log('> getCountries');
    return this.httpClient.get<Country[]>('http://localhost:8080/countries');
  }

  public sendToCalculate(calculateModel) {
    console.log('> sendToCalculate');
    return this.httpClient.post<Calculate>('http://localhost:8080/calculate', calculateModel);
  }

}
