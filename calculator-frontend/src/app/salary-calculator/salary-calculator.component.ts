import {Component, OnInit} from '@angular/core';
import {Calculate, Country, HttpClientService} from '../service/http-client.service';

@Component({
  selector: 'app-salary-calculator',
  templateUrl: './salary-calculator.component.html',
  styleUrls: ['./salary-calculator.component.scss']
})
export class SalaryCalculatorComponent implements OnInit {

  calculateModel: Calculate = new Calculate(0, '');

  resultCalculate: string;
  countries: Country[];
  countriesCalculatorError: boolean = false;

  constructor(private httpClientService: HttpClientService) {
  }

  ngOnInit(): void {
    this.httpClientService.getCountries().subscribe((data: Country[]) => {
        this.countries = data;
        this.calculateModel.codeCountry = this.countries[0].code;
      },
      (error: Error) => {
        this.countriesCalculatorError = true;
      });
  }


  onCalculate(): void {
    this.httpClientService.sendToCalculate(this.calculateModel).subscribe(
      (data) => {
        this.resultCalculate = Number(data).toFixed(2);
      },
      (error: Error) => {
        this.countriesCalculatorError = true;
      });
  }
}
