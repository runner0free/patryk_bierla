import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalaryCalculatorComponent } from './salary-calculator/salary-calculator.component';

const routes: Routes = [
  	{ path: '', redirectTo: '/salary-calculator', pathMatch: 'full'},
 	{ path:'salary-calculator', component: SalaryCalculatorComponent}
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
