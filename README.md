# Calculate salary project
Application calculate monthly net salary in countries like German, United Kingdom and Poland.

Application use currency rate from API (nbp.pl)

Application is done in technologies like SpringBoot and Angular

## Requirments
 Java 1.8 <br />
 Maven <br />
 Angular CLI: 8.0.1 <br />
 Node: 10.16.0 <br />


## Run application
Go to patryk_bierla/calculator execute from command line: <br />
mvn clean install <br />
Run backend application <br />
mvn spring-boot:run <br />

Go to patryk_bierla/calculator-frontend type: <br />
ng serve

### How to use
Open browser and go to http://localhost:4200