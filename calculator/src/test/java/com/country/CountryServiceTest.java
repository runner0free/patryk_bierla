package com.country;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class CountryServiceTest {

    CountryService countryService= new CountryService();



    @Test
    public void getCountryByCodeNull() {
        Country countryByCode = countryService.getCountryByCode(null);
        assertNull(countryByCode);
    }

    @Test
    public void getCountryByCode1() {
        Country countryByCode = countryService.getCountryByCode("UK");

        assertEquals("UK",countryByCode.getCode());
    }
}