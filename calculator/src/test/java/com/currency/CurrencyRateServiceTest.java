package com.currency;

import com.nbp.api.NbpAPIService;
import com.nbp.api.NbpRate;
import com.nbp.api.NbpTableRatesResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
public class CurrencyRateServiceTest {
    @Mock
    private NbpAPIService nbpAPI;

    @InjectMocks
    @Spy
    private CurrencyRateService currencyRateService;

    private NbpTableRatesResponse tableRateNBP;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        NbpRate nbpRateEuro=new NbpRate("euro","EUR",3.4D);
        NbpRate nbpRateFunt=new NbpRate("funt","GBP",3.777D);
        tableRateNBP=new NbpTableRatesResponse();
        tableRateNBP.setRates(new NbpRate[]{nbpRateEuro,nbpRateFunt});

    }
    @Test
    public void getCurrencyMidValueCheckNull1() {
        when(nbpAPI.getTableRateNBP()).thenReturn(null);

        double eur = currencyRateService.getCurrencyMidValue("EUR");

        assertEquals(0,eur,0);
    }
    @Test
    public void getCurrencyMidValueCheckNull2() {
        NbpTableRatesResponse tableRateNBP=new NbpTableRatesResponse();
        when(nbpAPI.getTableRateNBP()).thenReturn(tableRateNBP);

        double eur = currencyRateService.getCurrencyMidValue("EUR");

        assertEquals(0,eur,0);
    }


    @Test
    public void getCurrencyMidValue1() {
        when(nbpAPI.getTableRateNBP()).thenReturn(tableRateNBP);

        double eur = currencyRateService.getCurrencyMidValue("EUR");

        assertEquals(3.4D,eur,0);
    }
    @Test
    public void getCurrencyMidValue2() {
        when(nbpAPI.getTableRateNBP()).thenReturn(tableRateNBP);

        double eur = currencyRateService.getCurrencyMidValue("GBP");

        assertEquals(3.777D,eur,0);
    }
}