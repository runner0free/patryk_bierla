package com.calculate;

import com.country.Country;
import com.country.CountryService;
import com.currency.CurrencyRateService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CalculateServiceTest {
    @Mock
    CurrencyRateService currencyRateService;
    @Mock
    CountryService countryService;

    @Spy
    @InjectMocks
    CalculateService calculateService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void calculateSalaryNull() {
        double salary = calculateService.calculateSalary(100, "DE");
        assertEquals(0,salary, 0);

    }

    @Test
    public void calculateSalaryDE() {
        Country countryDE= new Country();
        countryDE.setCode("DE");
        countryDE.setName("Germany");
        countryDE.setTax(0.2);
        countryDE.setFixedCosts(800);
        countryDE.setCurrencyCode("EUR");
        when(countryService.getCountryByCode(countryDE.getCode())).thenReturn(countryDE);
        when(currencyRateService.getCurrencyMidValue(anyString())).thenReturn(3.9D);

        double salary = calculateService.calculateSalary(100, "DE");
        assertEquals(3744,salary, 0.00001);

    }

    @Test
    public void calculateSalaryUK() {
        Country countryUK= new Country();
        countryUK.setCode("UK");
        countryUK.setName("United Kingdom");
        countryUK.setTax(0.25);
        countryUK.setFixedCosts(600);
        countryUK.setCurrencyCode("GBP");
        when(countryService.getCountryByCode("UK")).thenReturn(countryUK);
        when(currencyRateService.getCurrencyMidValue(anyString())).thenReturn(3.777D);

        double salary = calculateService.calculateSalary(100, "UK");
        assertEquals(3965.85D,salary, 0.00001);

    }

    @Test
    public void calculateSalaryPL() {
        Country countryPL= new Country();
        countryPL.setCode("PL");
        countryPL.setName("Poland");
        countryPL.setTax(0.19);
        countryPL.setFixedCosts(1200);
        countryPL.setCurrencyCode("PLN");
        when(countryService.getCountryByCode("PL")).thenReturn(countryPL);
        when(currencyRateService.getCurrencyMidValue(anyString())).thenReturn(1D);

        double salary = calculateService.calculateSalary(100, "PL");
        assertEquals(582D,salary, 0.00001);

    }
}