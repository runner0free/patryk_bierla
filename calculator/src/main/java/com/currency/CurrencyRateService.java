package com.currency;

import com.nbp.api.NbpAPIService;
import com.nbp.api.NbpRate;
import com.nbp.api.NbpTableRatesResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class CurrencyRateService {

    Logger logger = LoggerFactory.getLogger(CurrencyRateService.class);

    @Autowired
    private NbpAPIService nbpAPI;

    public double getCurrencyMidValue(String currency) {
        logger.info("> getCurrencyMidValue currency: {}", currency);
        NbpTableRatesResponse tableRateNBP = nbpAPI.getTableRateNBP();
        if (tableRateNBP == null || tableRateNBP.getRates() == null) {
            logger.info("< getCurrencyMidValue tableRateNBP is null");
            return 0;
        }
        Double midNbpRate = Arrays.stream(tableRateNBP.getRates())
                .filter(p -> StringUtils.isNotEmpty(p.getCode()) && p.getCode().equals(currency))
                .map(NbpRate::getMid)
                .findAny()
                .orElse(0D);
        logger.info("< getCurrencyMidValue midNbpRate: {}", midNbpRate);
        return midNbpRate;
    }
}
