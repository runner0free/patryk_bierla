package com.country;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Country {
    private String code;
    private String name;
    private double tax;
    private int fixedCosts;
    private String currencyCode;
}
