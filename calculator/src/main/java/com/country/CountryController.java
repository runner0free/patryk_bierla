package com.country;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class CountryController {

    private Logger logger = LoggerFactory.getLogger(CountryController.class);

    @Autowired
    CountryService countryService;

    @GetMapping(path = "/countries", produces = "application/json")
    public List<Country> getCountries() {
        logger.info("> getCountries");
        List<Country> countries = countryService.getCountries();
        logger.info("< getCountries size: {}", countries.size());
        return countries;
    }


}
