package com.country;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CountryService {

    private List<Country> countries = createList();

    public List<Country> getCountries() {
        return countries;
    }

    public Country getCountryByCode(String code) {
        return countries.stream()
                .filter(p -> code != null && code.equals(p.getCode()))
                .findAny()
                .orElse(null);
    }

    private List<Country> createList() {
        countries = new ArrayList<>();

        Country countryUK = Country.builder()
                .code("UK")
                .name("United Kingdom")
                .tax(0.25)
                .fixedCosts(600)
                .currencyCode("GBP")
                .build();
        countries.add(countryUK);

        Country countryDE = Country.builder()
                .code("DE")
                .name("Germany")
                .tax(0.2)
                .fixedCosts(800)
                .currencyCode("EUR")
                .build();
        countries.add(countryDE);

        Country countryPL = Country.builder()
                .code("PL")
                .name("Poland")
                .tax(0.19)
                .fixedCosts(1200)
                .currencyCode("PLN")
                .build();
        countries.add(countryPL);

        return countries;
    }
}
