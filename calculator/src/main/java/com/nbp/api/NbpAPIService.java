package com.nbp.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class NbpAPIService {
    private Logger logger = LoggerFactory.getLogger(NbpAPIService.class);

    private static final String URL_API_NBP = "http://api.nbp.pl/api/exchangerates/tables/a/";

    public NbpTableRatesResponse getTableRateNBP(){
        logger.info("> getTableRateNBP");
        RestTemplate restTemplate = new RestTemplate();
        NbpTableRatesResponse[] response = restTemplate.getForObject(URL_API_NBP, NbpTableRatesResponse[].class);
        if (response == null || response[0] == null) {
            logger.info("< getTableRateNBP: " + response);
            return null;
        } else {
            logger.info("< getTableRateNBP:" + response.length);
            return response[0];
        }
    }
}
