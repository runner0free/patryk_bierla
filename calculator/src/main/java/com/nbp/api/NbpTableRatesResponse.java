package com.nbp.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NbpTableRatesResponse {
    private String table;
    private String no;
    private String effectiveDate;
    private NbpRate[] rates;
}
