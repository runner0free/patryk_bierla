package com.calculate;

import com.country.Country;
import com.country.CountryService;
import com.currency.CurrencyRateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalculateService {

    private Logger logger = LoggerFactory.getLogger(CalculateService.class);

    private static final int DAYS_IN_MONTH = 22;

    @Autowired
    CurrencyRateService currencyRateService;
    @Autowired
    CountryService countryService;

    public double calculateSalary(double dailyPayGross, String countryCode) {
        logger.info("> calculateSalary: dailyPayGross: {}, countryCode: {}", dailyPayGross, countryCode);
        Country country = countryService.getCountryByCode(countryCode);
        double currencyMidValue = 0;
        if (country == null) {
            logger.info("< calculateSalary is null value!");
            return 0;
        }
        currencyMidValue = currencyRateService.getCurrencyMidValue(country.getCurrencyCode());
        double salaryInPLN = getSalaryInPLN(dailyPayGross, country, currencyMidValue);
        logger.info("< salary calculated: {}", salaryInPLN);
        return  salaryInPLN;
    }

    private double getSalaryInPLN(double dailyPayGross, Country country, double currencyMidValue) {
        return getNetSalaryForeginCurrency(dailyPayGross, country) * currencyMidValue;
    }

    private double getNetSalaryForeginCurrency(double dailyPayGross, Country country) {

        return dailyPayGross * DAYS_IN_MONTH * (1 - country.getTax()) - country.getFixedCosts();
    }
}
