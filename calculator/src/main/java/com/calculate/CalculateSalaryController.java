package com.calculate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class CalculateSalaryController {

    private Logger logger = LoggerFactory.getLogger(CalculateSalaryController.class);

    @Autowired
    CalculateService calculateService;

    @PostMapping({ "/calculate" })
    public double calculate(@RequestBody Calculate calculate) {
        logger.info("> calculate ");
        double salaryCalculated = calculateService.calculateSalary(calculate.getDailyPayGross(), calculate.getCodeCountry());
        logger.info("< calculate: {}", salaryCalculated);
        return salaryCalculated;
    }


}
